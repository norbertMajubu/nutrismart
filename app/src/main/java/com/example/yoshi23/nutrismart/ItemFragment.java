package com.example.yoshi23.nutrismart;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yoshi23.nutrismart.Classes.DummyContent;
import com.example.yoshi23.nutrismart.Classes.DummyContent.DummyItem;
import com.example.yoshi23.nutrismart.Classes.Mealplan;
import com.example.yoshi23.nutrismart.Classes.MealplanHolder;
import com.example.yoshi23.nutrismart.Classes.Mealplan;
import com.example.yoshi23.nutrismart.Classes.MealplanHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ItemFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;

    public FragmentManager fragmentManager;

    protected static final Query sMealplanQuery =
            FirebaseDatabase.getInstance().getReference().child("mealplans").limitToLast(50);
    private ArrayList<String> mealplansToday;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemFragment(/*FragmentManager parentsfragmentManager*/) {
        //fragmentManager = parentsfragmentManager;
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    /*public static ItemFragment newInstance(int columnCount) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        Bundle bundle = this.getArguments();
        Log.d("bundledata", "this is the melplans to be displayed befre check");
        if (bundle != null) {
            mealplansToday = bundle.getStringArrayList("mealplanOfDay");
            Log.d("bundledata", "this is the melplans to be displayed11111: "+bundle.getStringArrayList("mealplanOfDay"));
            Log.d("bundledata", "this is the melplans to be displayed222: "+mealplansToday);

         //   weeklyPlanToDisplay = bundle.getString("weeklyPlanId", "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyItemRecyclerViewAdapter(DummyContent.ITEMS, mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }

    /**************FIREBASE UI ADAPTER ******************/
    @Override
    public void onStart() {
        super.onStart();
        //if (isSignedIn()) { attachRecyclerViewAdapter(); }
        attachRecyclerViewAdapter();
        //FirebaseAuth.getInstance().addAuthStateListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //FirebaseAuth.getInstance().removeAuthStateListener(this);
    }
    private void attachRecyclerViewAdapter() {
        final RecyclerView.Adapter adapter = newAdapter(mealplansToday);

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                recyclerView.smoothScrollToPosition(adapter.getItemCount());
            }
        });

        recyclerView.setAdapter(adapter);
    }



    protected RecyclerView.Adapter newAdapter(ArrayList<String> mealplansToday) {

        final ArrayList<String> mealplansToDisplay = mealplansToday;
    /*
=======
    protected RecyclerView.Adapter newAdapter(String currentWeeklyPlanId) {

        Query sMealplanQuery =
                FirebaseDatabase.getInstance().getReference().child("weeklyplans").orderByKey().equalTo(currentWeeklyPlanId);
        sMealplanQuery.addListenerForSingleValueEvent(



                //AZT OTT ALUL IDE KÉNE BEPAKOLNI ÉS AKKOR ITT HIVODNA MEG A BUILDING...AHOL AMÚGY LEGYTUDJUK KÉRNI A MEALPLANAKAT



        );
>>>>>>> Stashed changes*/
        FirebaseRecyclerOptions<Mealplan> options =
                new FirebaseRecyclerOptions.Builder<Mealplan>()
                        .setQuery(sMealplanQuery, Mealplan.class)
                        .setLifecycleOwner(this)
                        .build();

        return new FirebaseRecyclerAdapter<Mealplan, MealplanHolder>(options) {
            @Override
            public MealplanHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                fragmentManager = getActivity().getSupportFragmentManager();

                return new MealplanHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_item, parent, false), fragmentManager);
            }

            @Override
            protected void onBindViewHolder(@NonNull MealplanHolder holder, int position, @NonNull Mealplan model) {
                Log.d("bindviewholder", "bindview holder called, this is model: "+model.toString());

                //if not in the list (users mealplan) --> do not bind!!!
                if(mealplansToDisplay.contains(model.getId())){
                    holder.bind(model);
                }

            }

            @Override
            public void onDataChanged() {
                // If there are no weeklmodelyPlan messages, show a view that invites the user to add a message.
                //mEmptyListMessage.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
            }
        };
    }
}
