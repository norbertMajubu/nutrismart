package com.example.yoshi23.nutrismart.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WeeklyPlan extends AbstractWeeklyPlan{

   /* public enum Day_of_Week {
        1,2,3,4,5,6,7
    }*/


    String id;
    String name ;
    String description;

    Long carbs;
    Long fat;
    Long protein;
    Long energy;


    ArrayList<HashMap<String, String> > days;
    //Map<String,ArrayList<Map<String,String> > > days; // = new HashMap<Integer, Map<String,Boolean> >();
    //ArrayList<ArrayList<ArrayList<String> > > days;


    public Long getCarbs() {
        return carbs;
    }

    public Long getFat() {
        return fat;
    }

    public Long getProtein() {
        return protein;
    }

    public Long getEnergy() {
        return energy;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }


    public ArrayList<HashMap<String, String> > getDays()
    {

    //}
  //  public ArrayList<ArrayList<Map<String,String> > > getDays(){
   // public ArrayList<ArrayList<Map<String,String> > > getDays() {
        return days;
    }


    public String getCarbsString() {
        return String.valueOf(carbs);
    }

    public String getFatString() {
        return String.valueOf(fat);
    }

    public String getProteinString() {
        return String.valueOf(protein);
    }

    public String getEnergyString() {
        return String.valueOf(energy);
    }


    @Override
    public int hashCode(){return 10001;}
    @Override
    public boolean equals(Object obj){
        if (obj.getClass() == Mealplan.class )
        {return true;}
        else{ return false;}
    }


}
