package com.example.yoshi23.nutrismart;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextClock;
import android.widget.TextView;

import com.example.yoshi23.nutrismart.Classes.Mealplan;
import com.example.yoshi23.nutrismart.Classes.User;
import com.example.yoshi23.nutrismart.Classes.WeeklyPlan;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView message;

    private OnFragmentInteractionListener mListener;

    private UserDataManager userDataManager;
    FirebaseUser user;
    User localUser;
    final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    public BlankFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment newInstance(String param1, String param2) {
        BlankFragment fragment = new BlankFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_blank, container, false);

        View v = inflater.inflate(R.layout.fragment_blank, container, false);

        LinearLayout mealplanLayout = v.findViewById(R.id.mealplan_layout);
        mealplanLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ValueEventListener currentWeeklyPlan = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d("weeklyke","ondata change is clled at least");

                        for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                            WeeklyPlan weeklyPlan = childSnapshot.getValue(WeeklyPlan.class);

                            ItemFragment nextFrag= new ItemFragment();
                            FragmentManager fragmentManager = getFragmentManager();

                            Fragment fragment = new Fragment();
                            Bundle bundle = new Bundle();

                            ArrayList<String> mealplansOfDay = new ArrayList<>(weeklyPlan.getDays().get(1).keySet());

                            bundle.putStringArrayList("mealplanOfDay", mealplansOfDay );
                            bundle.putString("weeklyPlanId", weeklyPlan.getId());
                            nextFrag.setArguments(bundle);


                            fragmentManager.beginTransaction()
                                    .replace(R.id.main_container, nextFrag)
                                    .addToBackStack("dashboard")
                                    .commit();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };

                Query weeklyPlanQuery = FirebaseDatabase.getInstance().getReference().getRef().child("weeklyplans").
                        orderByChild("id").
                        equalTo(userDataManager.localUser.getCurrentWeeklyPlanId());
                weeklyPlanQuery.addListenerForSingleValueEvent(currentWeeklyPlan);



            }
        });

        ImageButton b = v.findViewById(R.id.add_water);
        message  =  v.findViewById(R.id.message);
        b.setOnClickListener(this);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart(){
        super.onStart();


        ProgressBar progressBar = getActivity().findViewById(R.id.progress_bar_water);
        progressBar.setMax(2000); //2000ml as 2l


        ImageButton imageButton = getActivity().findViewById(R.id.add_water);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("imagbut","called");
                userDataManager.add100mlWater();
            }
        });




        populateProfile();

    }

    @Override
    public void onResume(){
        super.onResume();
        populateProfile();

    }


    private void populateProfile() {
        user = FirebaseAuth.getInstance().getCurrentUser();
        userDataManager = new UserDataManager(user);
        ImageView imageView = getActivity().findViewById(R.id.profile_pic);
        // MenuItem menuItem = findViewById(R.id.profile_pic_toolbar);

        if (user != null){
            if (user.getPhotoUrl() != null) {
                Picasso.get().load(user.getPhotoUrl() ).into(imageView);
                // Picasso.get().load(user.getPhotoUrl() ).into(imageView_toolbar);
            }

            TextView userDisplayName = getActivity().findViewById(R.id.userDisplayName);
            userDisplayName.setText(
                    TextUtils.isEmpty(user.getDisplayName()) ? "No display name" : user.getDisplayName());

           // String uid  = user.getUid();

            //TODO STEPCOUNTER: https://www.youtube.com/watch?v=CNGMWnmldaU


           /* Query userQuery = FirebaseDatabase.getInstance().getReference().getRef().child("users").
                    orderByChild("uid").
                    equalTo(uid);
            userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ProgressBar progressBar = getActivity().findViewById(R.id.progress_bar_water);
                    User waterUser = dataSnapshot.getValue(User.class);
                    Integer progressStatus = waterUser.water.get(waterUser.);
                    progressBar.setProgress(progressStatus);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });*/

        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_water :
                AuthUI.getInstance()
                        .signOut(getContext())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                // user is now signed out
                                startActivity(new Intent(getContext(), LoginActivity.class));
                               // finish();
                            }
                        });
                /*Context context = getActivity().getApplicationContext();// getApplicationContext();

                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, "nyyyomii", duration);
                toast.show();
                //((MainActivity)getActivity()).startDownload(getView());
                List<String> results = ((MainActivity)getActivity()).getIngredients();
                this.message.setText(results.get(0));*/
        }
    }



    private class UserDataManager{
        FirebaseUser user;
        User localUser;
        Query userQuery;
        Query weeklyPlanQuery = null;

        UserDataManager(FirebaseUser iUser){
            Log.d("debugging","constructor of userdatamanager is called wit this user id: "+iUser.getUid());
            // localUser
            user = iUser;
            userQuery = FirebaseDatabase.getInstance().getReference().getRef().child("users").
                orderByChild("uid").
                equalTo(user.getUid());

            final ValueEventListener weeklyPlanListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d("weekly","ondata change is clled at least");
                    ImageView weeklyPlanImageView = getActivity().findViewById(R.id.dashboard_mealplan_pic);
                    TextView weeklyPlantextView = getActivity().findViewById(R.id.dashboard_weeklyplan_name);
                    TextView weeklyPlanDescriptionView = getActivity().findViewById(R.id.dashboard_weeklyplan_description);

                    TextView mealplantextView = getActivity().findViewById(R.id.dashboard_mealplan_name);

                    for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                        WeeklyPlan weeklyPlan = childSnapshot.getValue(WeeklyPlan.class);
                        Log.d("weekly","name of the mealplan: "+weeklyPlan.getName());
                        weeklyPlantextView.setText("This week's plan: " + weeklyPlan.getName());
                        weeklyPlanDescriptionView.setText(weeklyPlan.getDescription());
                        Picasso.get().load("https://img.grouponcdn.com/deal/3ENxPotH3vMNhQ7NixTCTYgtFJcD/3E-2048x1229/v1/c700x420.jpg").into(weeklyPlanImageView);

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };

           ValueEventListener valueEventListener = new ValueEventListener() {
               @Override
               public void onDataChange(DataSnapshot dataSnapshot) {
                   Log.d("debugging","on data change called");
                   if(dataSnapshot.exists())
                   {
                       for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                           Log.d("debugg", "hwwwwwwwwwwwwwwww: " + childSnapshot.hasChild("weeklyplans"));
                           UserDataManager.this.localUser = childSnapshot.getValue(User.class);
                           UserDataManager.this.localUser.displayName = (String) childSnapshot.child("displayName").getValue();

                           if (weeklyPlanQuery == null)
                           {
                               Log.d("debugg", "weeklyplanqquery was null, now it is called. This is the id of mealplan: "+localUser.getCurrentWeeklyPlanId());
                               weeklyPlanQuery = FirebaseDatabase.getInstance().getReference().getRef().child("weeklyplans").
                                       orderByChild("id").
                                       equalTo(localUser.getCurrentWeeklyPlanId());
                               weeklyPlanQuery.addListenerForSingleValueEvent(weeklyPlanListener);
                           }


                           Log.d("debuggging", "value of localuser: " + UserDataManager.this.localUser.displayName);

                           ProgressBar progressBar = getActivity().findViewById(R.id.progress_bar_water);

                           Integer progressStatus = UserDataManager.this.localUser.water.get(UserDataManager.this.localUser.water.size() - 1);

                           progressBar.setProgress(progressStatus); //, true);
                       }
                   }
                   else{
                       Log.d("debugging","shaaaaaaalom no datasnapshot");
                       ProgressBar progressBar = getActivity().findViewById(R.id.progress_bar_water);

                       progressBar.setProgress(0); //, true);
                   }
               }

               @Override
               public void onCancelled(DatabaseError databaseError) {
                   Log.d("debugging", "oncancelled called");
               }
           };



            userQuery.addListenerForSingleValueEvent(valueEventListener);

        }


        void add100mlWater(){
            userQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d("debugging", "on data change called");
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                            Log.d("debugging", "value of localuser: " + UserDataManager.this.localUser.displayName);
                            ArrayList<Integer> waterList = childSnapshot.getValue(User.class).water;
                            Integer newWater = waterList.get(waterList.size() - 1) + 100;
                            UserDataManager.this.localUser.water.set(UserDataManager.this.localUser.water.size() - 1, newWater);
                            mDatabase.child("users").child(user.getUid()).child("water").setValue(UserDataManager.this.localUser.water);


                            ProgressBar progressBar = getActivity().findViewById(R.id.progress_bar_water);

                            Integer progressStatus = UserDataManager.this.localUser.water.get(UserDataManager.this.localUser.water.size() - 1);

                            progressBar.setProgress(progressStatus); //, true);

                        }
                    } else {
                        Log.d("debugging", "shaaaaaaalom no datasnapshot");
                        ProgressBar progressBar = getActivity().findViewById(R.id.progress_bar_water);

                        progressBar.setProgress(0); //, true);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d("debugging", "oncancelled called");
                }
            });


        }


    }

}
