package com.example.yoshi23.nutrismart.Classes;

public abstract class AbstractMealplan {
    /**
     * Common interface for chat messages, helps share code between RTDB and Firestore examples.
     */


        public abstract String getName();

        public abstract String getId();

        @Override
        public abstract int hashCode();

        @Override
        public abstract boolean equals(Object obj);


}
