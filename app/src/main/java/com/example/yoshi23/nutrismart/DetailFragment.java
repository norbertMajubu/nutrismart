package com.example.yoshi23.nutrismart;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yoshi23.nutrismart.Classes.WeeklyPlan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.*;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private  String inheritedContent;

    protected static Query sWeeklyPlanQuery;

    private OnFragmentInteractionListener mListener;

    public DetailFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance(String param1, String param2) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            String displayedContent = getArguments().getString("displayedContent");
            inheritedContent = displayedContent;
        }
        sWeeklyPlanQuery = FirebaseDatabase.getInstance().getReference().
                child("weeklyPlans").
                orderByChild("name").
                equalTo(inheritedContent);
        if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    private class myValueEventListener implements ValueEventListener {
        ImageView imageView;
        TextView textViewName;
        TextView textViewCarbs;
        TextView textViewFat;
        TextView textViewProtein;
        TextView textViewEnergy;

        myValueEventListener(ImageView _imageView,
                             TextView _textViewName,
                             TextView _textViewCarbs,
                             TextView _textViewFat,
                             TextView _textViewProtein,
                             TextView _textViewEnergy){
            imageView = _imageView;
            textViewName = _textViewName;
            textViewCarbs = _textViewCarbs;
            textViewFat = _textViewFat;
            textViewProtein = _textViewProtein;
            textViewEnergy = _textViewEnergy;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                // dataSnapshot is the "issue" node with all children with id 0
                for (DataSnapshot issue : dataSnapshot.getChildren()) {
                    // do something with the individual "issues"
                    WeeklyPlan weeklyPlan = issue.getValue(WeeklyPlan.class);
                    Picasso.get().load("https://canales.okdiario.com/recetas/img/2016/10/03/cuscus-1-1.jpg").into(imageView);
                    textViewName.setText(weeklyPlan.getName());
                    textViewCarbs.setText(weeklyPlan.getCarbsString());
                    textViewFat.setText(weeklyPlan.getFatString());
                    textViewProtein.setText(weeklyPlan.getProteinString());
                    textViewEnergy.setText(weeklyPlan.getEnergyString());
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        ImageView imageView = getActivity().findViewById(R.id.detail_weeklyPlan_picture);
        TextView textViewName =  getActivity().findViewById(R.id.detail_weeklyPlan_name);
        TextView textViewCarbs =  getActivity().findViewById(R.id.detail_weeklyPlan_carbs);
        TextView textViewFat =  getActivity().findViewById(R.id.detail_weeklyPlan_fat);
        TextView textViewProtein =  getActivity().findViewById(R.id.detail_weeklyPlan_protein);
        TextView textViewEnergy =  getActivity().findViewById(R.id.detail_weeklyPlan_energy);

        sWeeklyPlanQuery.addListenerForSingleValueEvent(new myValueEventListener(imageView,
                textViewName,
                textViewCarbs,
                textViewFat,
                textViewProtein,
                textViewEnergy));

        Button button = getActivity().findViewById(R.id.weeklyPlan_details_goback_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Fragment listFragment  = fragmentManager.findFragmentByTag("weeklyPlan_list");
                fragmentManager.beginTransaction()
                        .replace(R.id.main_container, listFragment)
                        .addToBackStack(null)
                        .commit();

                Context context = getContext();
                CharSequence text = "diiiiiicsssaaakakk ... ";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
