package com.example.yoshi23.nutrismart.Classes;


import java.util.ArrayList;

public class Mealplan extends AbstractMealplan {
    public final String id;
    public final String mealName;
    public final String name;

    public Long carbs;
    public Long fat;
    public Long protein;
    public Long energy;
    public String unit;

    ArrayList<String> ingredients;
    ArrayList<String> recipes;


    public Mealplan() {
        this.id = "0";
        this.mealName = "blank";
        this.name = "blank";
        this.carbs = Long.valueOf(0);
        this.fat = Long.valueOf(0);
        this.protein = Long.valueOf(0);
       // this.energy = 0;
        this.unit = "blank";

    }

   /* public Mealplan(String id, String mealName, int carbs, int fat, int protein, String unit) {
        this.id = id;
        this.mealName = mealName;
        this.carbs = carbs;
        this.fat = fat;
        this.protein = protein;
        this.energy = 10 * this.fat + 4 * this.carbs + 4*this.protein;
        this.unit = unit;

    }*/

    public Mealplan(String id, String mealName, String name, Long carbs, Long fat, Long protein, String unit, ArrayList<String> ingredients, ArrayList<String> recipes) {
        this.id = id;
        this.mealName = mealName;
        this.name = name;
        this.carbs = carbs; //Integer.parseInt(carbs);
        this.fat = fat; //Integer.parseInt(fat);
        this.protein = protein; // Integer.parseInt(protein);
        //this.energy = 10 * this.fat + 4 * this.carbs + 4*this.protein;
        this.unit = unit;

        this.ingredients = ingredients;
        this.recipes = recipes;

    }

    @Override
    public String toString() {
        return this.mealName;
    }


    @Override
    public int hashCode(){return 10001;}
    @Override
    public boolean equals(Object obj){
        if (obj.getClass() == Mealplan.class )
        {return true;}
        else{ return false;}
    }

    public void setUnit(String new_unit){ unit = new_unit;}
   /* public void setCarbs(int new_carbs){ carbs = new_carbs;}
    public void setProtein(int new_protein){ protein = new_protein;}
    public void setFat(int new_fat){ fat = new_fat;}*/

    public String getName(){ return mealName;}
    public String getId(){ return id;}
    public String getUnit(){ return unit;}
    public Long getCarbs(){ return carbs;}
    public Long getProtein(){ return protein;}
    public Long getFat(){ return fat;}
    public Long getEnergy(){return energy;}
    public String getMealName(){return mealName;}

    public String getCarbsString(){ return String.valueOf(carbs);}
    public String getProteinString(){ return String.valueOf(protein);}
    public String getFatString(){ return String.valueOf(fat);}
    public String getEnergyString(){return String.valueOf(energy);}

}