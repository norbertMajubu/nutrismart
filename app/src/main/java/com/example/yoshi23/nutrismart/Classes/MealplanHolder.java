package com.example.yoshi23.nutrismart.Classes;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import android.widget.TextView;

import com.example.yoshi23.nutrismart.DetailFragment;
import com.example.yoshi23.nutrismart.R;

public class MealplanHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private final TextView mNameField;
    private final TextView mTextField;

    private String displayed_content;

    FragmentManager fragmentManager;


    public MealplanHolder(View itemView, FragmentManager activitiesFragmentManager) {
        super(itemView);
        mNameField = itemView.findViewById(R.id.content);
        mTextField = itemView.findViewById(R.id.item_number);

        itemView.setOnClickListener(this);

        fragmentManager = activitiesFragmentManager;
    }

    @Override
    public void onClick(View view) {
        //https://stackoverflow.com/questions/21028786/how-do-i-open-a-new-fragment-from-another-fragment
        DetailFragment nextFrag= new DetailFragment(); //mNameField.getText());
        Bundle args = new Bundle();
        args.putString("displayedContent", displayed_content);
        nextFrag.setArguments(args);
        fragmentManager.beginTransaction()
                .replace(R.id.main_container, nextFrag)
                .addToBackStack("ingredient_list")
                .commit();

        setName("NNNNZZZZAAAA"); //ingredient.getName());
        Log.d("ingredient", "onClick calle for ingredient holder");
    }

    public void bind(AbstractMealplan ingredient) {
        setName(ingredient.getName());
        setText(ingredient.getId());
        displayed_content = ingredient.getName();
    }

    private void setName(String name) {
        mNameField.setText(name);
    }

    private void setText(String text) {
        mTextField.setText(text);
    }
}
