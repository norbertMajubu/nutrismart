package com.example.yoshi23.nutrismart.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class User {

    public String uid;
    public String displayName;
    public String firstname;
    public String surname;
    public String email;
    public String photoURL;
    public String test;
    public HashMap<String,String> weeklyplans;

    public ArrayList<Integer> water;
    public ArrayList<Integer> steps;
    public ArrayList<Integer> weight;

    boolean trainer;

    public User(){
        water = new ArrayList<Integer>(0);
        water.add(0);
        steps = new ArrayList<Integer>(0);
        steps.add(0);
        weight = new ArrayList<Integer>(0);
        weight.add(0);
        trainer =false;

        //TODO MOCK MAKE IT DYNAMIC
        weeklyplans = new HashMap<String,String>();
        weeklyplans.put("-LCce3wWwm0lvmouBetS","true");
    }

    public User(    String iuid,
             String idisplayName,
             String ifirstname,
             String isurname,
             String iemail,
             String iphotoURL){
        uid = iuid;
        displayName = idisplayName;
        firstname = ifirstname;
        surname = isurname;
        email = iemail;
        photoURL = iphotoURL;
        trainer =false;

        water = new ArrayList<Integer>(0);
        water.add(0);
        steps = new ArrayList<Integer>(0);
        steps.add(0);
        weight = new ArrayList<Integer>(0);
        weight.add(0);

        //TODO MOCK MAKE IT DYNAMIC
        weeklyplans = new HashMap<String,String>();
        weeklyplans.put("-LCce3wWwm0lvmouBetS","true");

    }

    public String getUid() {
        return uid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public boolean getTrainer(){
        return trainer;
    }

    public ArrayList<Integer> getSteps() {
        return steps;
    }

    public ArrayList<Integer> getWater() {
        return water;
    }

    public ArrayList<Integer> getWeight() {
        return weight;
    }

    public HashMap<String,String>  getWeeklyplans() {
        return weeklyplans;
    }



    public String getCurrentWeeklyPlanId(){
        //HashMap mealplanIdMap = weeklyplans.get(weeklyplans.size()-1);
        Set keySet = weeklyplans.keySet();
        String mealplanId = "";
        for (Object key :  keySet){
            mealplanId = key.toString();
        }
        return mealplanId;

    }




    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public void setSteps(ArrayList<Integer> steps) {
        this.steps = steps;
    }

    public void setTrainer(boolean trainer) {
        this.trainer = trainer;
    }

    public void setWater(ArrayList<Integer> water) {
        this.water = water;
    }

    public void setWeeklyplans( HashMap<String,String>  weeklyplans) {
        this.weeklyplans = weeklyplans;
    }

    public void setWeight(ArrayList<Integer> weight) {
        this.weight = weight;
    }
}
