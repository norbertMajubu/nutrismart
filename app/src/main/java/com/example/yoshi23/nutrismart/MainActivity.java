package com.example.yoshi23.nutrismart;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yoshi23.nutrismart.Classes.DummyContent;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity implements
        ItemFragment.OnListFragmentInteractionListener,
        BlankFragment.OnFragmentInteractionListener,
        DetailFragment.OnFragmentInteractionListener
{
    private Fragment fragment;
    private FragmentManager fragmentManager;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    fragment = new BlankFragment();
                    transaction.replace(R.id.main_container, fragment);
                    transaction.commit();
                    return true;
                /*case R.id.navigation_monitor:
                    fragment = new PlusOneFragment();
                    transaction.replace(R.id.main_container, fragment);
                    //transaction.remove(R.id.main_container);
                    transaction.commit();
                    return true;*/
                case R.id.navigation_nutrition:
                    fragment = new ItemFragment(); //getSupportFragmentManager());
                    transaction.replace(R.id.main_container, fragment, "weeklyPlan_list");
                    transaction.commit();
                    return true;
                case R.id.navigation_shopping_list:
                    fragment = new BlankFragment();
                    transaction.replace(R.id.main_container, fragment);
                    transaction.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_main);
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        fragment = new BlankFragment();
        setTitle("NutriSmart");
        FrameLayout ll = (FrameLayout) findViewById(R.id.main_container);
        ll.removeAllViews();
        transaction.replace(R.id.main_container,fragment);
        transaction.commit();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


    }

    @Override
    protected  void onStart(){
        super.onStart();
       // populateProfile();
    }

    @Override
    public void onResume(){
        super.onResume();
       // populateProfile();auth.FirebaseUser.getUid(

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case 1: //R.id.action_add:
               // addSomething();
                return true;
            case 2:// R.id.action_settings:
                //startSettings();
                return true;
            case R.id.signout:
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                // user is now signed out
                                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                // finish();
                            }
                        });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        TextView textView = new TextView(this);
        textView.setText(title);

        //https://stackoverflow.com/questions/16901930/memory-leaks-with-custom-font-for-set-custom-font/16902532#16902532
        // //Typeface f = Typeface.create("Roboto");
        //Typeface face = Typeface.createFromAsset(getAssets(),
        //       "qhyts___.ttf");
        //textView.setTypeface(f);
        //// https://stackoverflow.com/questions/16901930/memory-leaks-with-custom-font-for-set-custom-font/16902532#16902532
        //https://stackoverflow.com/questions/16901930/memory-leaks-with-custom-font-for-set-custom-font/16902532#16902532
        textView.setTextSize(30);
        textView.setTextColor(Color.WHITE);

        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(textView);
    }


   /* private void populateProfile() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        ImageView imageView = findViewById(R.id.profile_pic);
       // MenuItem menuItem = findViewById(R.id.profile_pic_toolbar);

        if (user != null){
            if (user.getPhotoUrl() != null) {
                Picasso.get().load(user.getPhotoUrl() ).into(imageView);
               // Picasso.get().load(user.getPhotoUrl() ).into(imageView_toolbar);
            }

            TextView userDisplayName = findViewById(R.id.userDisplayName);
            userDisplayName.setText(
                    TextUtils.isEmpty(user.getDisplayName()) ? "No display name" : user.getDisplayName());

        }
    }*/




    @Override
    public void onFragmentInteraction(Uri uri){}

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item){
        Context context = getApplicationContext();
        CharSequence text = "Hello toast!  ... " + item.id.toString();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
